# LLBLGen Self Servicing Publishers Libraries

Supporting of LLBLGen O/R mapping classes.

* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories] are required:
* [Core Libraries] - Core Libraries
* [Data Libraries] - Data Libraries
* [Publishers Libraries] - Data Publishers Libraries
* [IDE Repository] - IDE support files. 
* [WiX Repository] - WiX Installer files.  
```
git clone git@bitbucket.org:davidhary/vs.core.git
git clone git@bitbucket.org:davidhary/vs.data.llblgen.git
git clone git@bitbucket.org:davidhary/vs.data.llblgen.publishers.git
git clone git@bitbucket.org:davidhary/vs.ide.git
git clone git@bitbucket.org:davidhary/vs.wix.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
%vslib%\core\core
%vslib%\data\llblgen
%vslib%\data\llblgen.publishers
%vslib%\core\ide
%vslib%\core\wix
```
where %dnlib% and %vslib% are  the root folders of the .NET libraries, e.g., %my%\lib\vs 
and %my%\libraries\vs, respectively, and %my% is the root folder of the .NET solutions

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

<a name="FacilitatedBy"></a>
## Facilitated By
* [Visual Studio] 
* [Jarte RTF Editor] 
* [Wix Toolset]
* [Atomineer Code Documentation]
* [EW Software Spell Checker]
* [Code Converter]
* [Search and Replace]
* [LLBLGen] - O/R Mapping

<a name="Repository-Owner"></a>
## Repository Owner
[ATE Coder]

<a name="Authors"></a>
## Authors
* [ATE Coder]  

<a name="Acknowledgments"></a>
## Acknowledgments
* [Its all a remix] -- we are but a spec on the shoulders of giants  
* [John Simmons] - outlaw programmer  
* [Stack overflow] - Joel Spolsky  
* [.Net Foundation] - The .NET Foundation

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries]
[Data Libraries]

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries]  
[LLBLGEN] - By Solution Design SV

[Core Libraries]: https://bitbucket.org/davidhary/dn.core
[Data Libraries]: https://www.bitbucket.org/davidhary/vs.data
[SQL Connection Dialog]: https://www.codeproject.com/tips/857399/sql-server-connection-dialog-extensible
[LLBLGEN]: http://www.llblger.com
[Data Libraries]: https://www.bitbucket.org/davidhary/vs.data.llblgen
[Publishers Libraries]: https://www.bitbucket.org/davidhary/vs.data.llblgen.publishers

[Microsoft /.NET Framework]: https://dotnet.microsoft.com/download

[external repositories]: ExternalReposCommits.csv
[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide
[WiX Repository]: https://www.bitbucket.org/davidhary/vs.wix

[ATE Coder]: https://www.IntegratedScientificResources.com
[Its all a remix]: https://www.everythingisaremix.info
[John Simmons]: https://www.codeproject.com/script/Membership/View.aspx?mid=7741
[Stack overflow]: https://www.stackoveflow.com

[Visual Studio]: https://www.visualstudio.com/
[Jarte RTF Editor]: https://www.jarte.com/ 
[WiX Toolset]: https://www.wixtoolset.org/
[Atomineer Code Documentation]: https://www.atomineerutils.com/
[EW Software Spell Checker]: https://github.com/EWSoftware/VSSpellChecker/wiki/
[Code Converter]: https://github.com/icsharpcode/CodeConverter
[Funduc Search and Replace]: http://www.funduc.com/search_replace.htm
[.Net Foundation]: https://source.dot.net
[Chris Winkelmann - SQL Connection Dialog]: https://www.codeproject.com/tips/857399/sql-server-connection-dialog-extensible

