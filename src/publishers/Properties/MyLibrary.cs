
namespace isr.Data.LLBLGen.Publishers.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    public sealed partial class MyLibrary
    {

        /// <summary> Constructor that prevents a default instance of this class from being created. </summary>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) ProjectTraceEventId.Publishers;
        /// <summary>   (Immutable) the assembly title. </summary>
        public const string AssemblyTitle = "LLBLGen Publishers Library";
        /// <summary>   (Immutable) information describing the assembly. </summary>
        public const string AssemblyDescription = "LLBLGen O/R Mapping Publishers Library";
        /// <summary>   (Immutable) the assembly product. </summary>
        public const string AssemblyProduct = "LLBLGen.Publishers";
    }

    /// <summary> Values that represent project trace event identifiers. </summary>
    public enum ProjectTraceEventId
    {
        /// <summary>   An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "Not specified" )]
        None,

        /// <summary>   An enum constant representing the publishers option. </summary>
        [System.ComponentModel.Description( "LLBLGen Publishers Library" )]
        Publishers = LLBLGen.My.ProjectTraceEventId.LLBLGenCore + 0x1,

        /// <summary>   An enum constant representing the self servicing option. </summary>
        [System.ComponentModel.Description( "LLBLgen Self Servicing Publishers" )]
        SelfServicing = Publishers + 0x1,

        /// <summary>   An enum constant representing the publishers tester option. </summary>
        [System.ComponentModel.Description( "LLBLgen Publishers Tester" )]
        PublishersTester = Publishers + 0xA
    }
}
