﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Data.LLBLGen.Publishers.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Data.LLBLGen.Publishers.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Data.LLBLGen.Publishers.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
