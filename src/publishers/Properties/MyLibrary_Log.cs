﻿using System;
using System.Diagnostics;

using isr.Core;
using isr.Data.LLBLGen.Publishers.ExceptionExtensions;

namespace isr.Data.LLBLGen.Publishers.My
{
    public sealed partial class MyLibrary
    {

        /// <summary> Logs unpublished exception. </summary>
        /// <param name="activity">  The activity. </param>
        /// <param name="exception"> The exception. </param>
        public static void LogUnpublishedException( string activity, Exception exception )
        {
            _ = LogUnpublishedMessage( new TraceMessage( TraceEventType.Error, TraceEventId, $"Exception {activity};. {exception.ToFullBlownString()}" ) );
        }

        /// <summary> Applies the given value. </summary>
        /// <param name="value"> The value. </param>
        public static void Apply( Logger value )
        {
            _Logger = value;
        }

        /// <summary> Applies the trace level described by value. </summary>
        /// <param name="value"> The value. </param>
        public static void ApplyTraceLogLevel( TraceEventType value )
        {
            TraceLevel = value;
            Logger.ApplyTraceLevel( value );
        }

        /// <summary> Applies the trace level described by value. </summary>
        public static void ApplyTraceLogLevel()
        {
            ApplyTraceLogLevel( TraceLevel );
        }
    }
}