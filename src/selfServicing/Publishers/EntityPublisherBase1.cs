using SD.LLBLGen.Pro.ORMSupportClasses;

namespace isr.Data.LLBLGen.SelfServicing
{
    /// <summary> Base class for anonymously publishing entity information for an entity that has a
    /// primary key consisting of one field. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2010-10-03, 1.0.3928.x. </para></remarks>
    public abstract class EntityPublisherBase1<TEntity, TPrimaryKey> : EntityPublisherBase<TEntity> where TEntity : EntityBase, IEntity
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs the class. </summary>
        protected EntityPublisherBase1() : base()
        {
        }

        #endregion

        #region " ENTITY BASE METHOD - NO EVENTS "

        /// <summary> Fetches an entity or clears exiting entity. </summary>
        /// <param name="primaryKey"> Specifies the first primary key. </param>
        /// <returns> <c>True</c> if entity exists. Otherwise, a new entity is created. </returns>
        /// <remarks> Raises the <see cref="Data.LLBLGen.Publishers.EntityPublisherBase.EntityCreated">entity created</see> event. Does not raise
        /// the <see cref="Publishers.EntityPublisherBase.EntityChanged">entity changed</see> event. </remarks>
        protected abstract bool FetchEntity( TPrimaryKey primaryKey );

        #endregion

        #region " ENTITY "

        /// <summary> Fetches a new or exiting adapter using the primary key. </summary>
        /// <param name="primaryKey"> Specifies a adapter number. </param>
        /// <returns> <c>True</c> if entity exists. Otherwise, a new entity is created. </returns>
        public virtual bool Fetch( TPrimaryKey primaryKey )
        {
            _ = this.FetchEntity( primaryKey );
            this.OnEntityChanged();
            return !this.Entity.IsNew;
        }

        /// <summary> Checks if an entity exists. </summary>
        /// <remarks> Find does not trigger the entity changed event. </remarks>
        /// <param name="primaryKey"> Specifies the primary key. </param>
        /// <returns> <c>True</c> if entity exists. </returns>
        public abstract bool Exists( TPrimaryKey primaryKey );

        /// <summary> Checks if a new entity needs to be fetched. </summary>
        /// <param name="primaryKey"> The primary key. </param>
        /// <returns> <c>True</c> if a new entity needs to be fetched; otherwise, <c>False</c>. </returns>
        public abstract bool IsFetchRequired( TPrimaryKey primaryKey );

        #endregion

    }
}
