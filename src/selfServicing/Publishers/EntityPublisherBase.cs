using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

using isr.Data.LLBLGen.Publishers;
using isr.Data.LLBLGen.SelfServicing.ExceptionExtensions;
using isr.Data.LLBLGen.SelfServicingEntityExtensions;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace isr.Data.LLBLGen.SelfServicing
{
    /// <summary> Base class for publishing entity information. </summary>
    /// <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2010-10-03, 1.0.3928.x. </para></remarks>
    public abstract class EntityPublisherBase<TEntity> : EntityPublisherBase where TEntity : EntityBase, IEntity
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs the class. </summary>
        protected EntityPublisherBase() : base()
        {
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                          <c>False</c> to release only unmanaged resources when called from the
        ///                          runtime finalize. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.EntityInternal = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " O/R MAPPING "

        /// <summary> Builds prefetch path for the entity. </summary>
        /// <returns> The <see cref="SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath">prefetch path</see> </returns>
        protected abstract IPrefetchPath BuildPrefetchPath();

        #endregion

        #region " ENTITY "


        private TEntity EntityInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get; [MethodImpl( MethodImplOptions.Synchronized )]
            set;
        }

        /// <summary> Gets a reference to the entity. </summary>
        /// <value> The entity. </value>
        public TEntity Entity => this.EntityInternal;

        #endregion

        #region " ENTITY NO PUBLISH "

        /// <summary> Saves the active entity without publishing events. </summary>
        /// <returns> <c>True</c> if entity was save and successfully refetched. </returns>
        public override bool SaveEntity()
        {
            string fieldNames = string.Join( ",", this.Entity.GetPrimaryKeyFieldNames().ToArray() );
            _ = (this.Talker?.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Saving entity; {0} '{1}'", this.Entity, fieldNames ));
            if ( this.Entity.Save() )
            {
                // refetch to keep entity active without raising events.
                if ( this.RefetchEntity() )
                {
                    return true;
                }
                else
                {
                    _ = (this.Talker?.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed fetching entity after saving;. '{0}' '{1}' from the database after saving.", this.Entity, fieldNames ));
                    return false;
                }
            }
            else
            {
                _ = (this.Talker?.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed saving entity;. '{0}' '{1}' to the database.", this.Entity, fieldNames ));
                return false;
            }
        }

        /// <summary> Sets the entity. </summary>
        /// <param name="value"> The value. </param>
        public void EntitySetter( TEntity value )
        {
            this.EntityInternal = value;
            base.EntitySetter( value );
        }

        #endregion

        #region " ENTITY WITH PUBLISH "

        /// <summary> Deletes the selected entity. </summary>
        /// <remarks> Raises the <see cref="EntityPublisherBase.EntityCreated">entity created</see> and the
        /// <see cref="EntityPublisherBase.EntityChanged">entity changed</see> events. </remarks>
        /// <param name="transaction"> The transaction. </param>
        /// <returns> <c>True</c> if entity was deleted. </returns>
        public override bool Delete( TransactionBase transaction )
        {
            bool result = false;
            if ( this.Entity is null || this.Entity.IsNew )
            {
                result = true;
            }
            else
            {
                if ( transaction is null )
                {
                    throw new ArgumentNullException( nameof( transaction ) );
                }

                try
                {
                    transaction.Add( this.Entity );
                    result = this.Entity.Delete();
                    transaction.Commit();
                }
                catch ( ORMQueryExecutionException ex )
                {
                    transaction.Rollback();
                    _ = (this.Talker?.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Exception occurred deleting entity;. '{0}'{1}{2}", this.Entity, ex.QueryExecuted, ex.ToFullBlownString() ));
                }
                finally
                {
                    if ( result )
                    {
                        this.CreateEntity( true );
                        this.OnEntityChanged();
                    }
                }
            }

            return result;
        }

        /// <summary> Saves the active entity. </summary>
        /// <returns> <c>True</c> if entity was save and successfully refetched. </returns>
        public override bool Save()
        {
            if ( this.SaveEntity() )
            {
                // raise the saved and let the calling application decide if need to do entity changed and entities changed.
                this.OnEntitySaved();
                return true;
            }

            return false;
        }

        #endregion

    }
}
