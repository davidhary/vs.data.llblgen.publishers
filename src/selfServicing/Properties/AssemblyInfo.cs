﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Data.LLBLGen.SelfServicing.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Data.LLBLGen.SelfServicing.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Data.LLBLGen.SelfServicing.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
