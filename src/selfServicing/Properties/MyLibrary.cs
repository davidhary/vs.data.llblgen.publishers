
namespace isr.Data.LLBLGen.SelfServicing.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    public sealed partial class MyLibrary
    {

        /// <summary> Constructor that prevents a default instance of this class from being created. </summary>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) Publishers.My.ProjectTraceEventId.SelfServicing;
        /// <summary>   (Immutable) the assembly title. </summary>
        public const string AssemblyTitle = "LLBLGen Self Servicing Library";
        /// <summary>   (Immutable) information describing the assembly. </summary>
        public const string AssemblyDescription = "LLBLGen O/R Mapping Self Servicing Library";
        /// <summary>   (Immutable) the assembly product. </summary>
        public const string AssemblyProduct = "LLBLGen.SelfServicing";
    }
}
