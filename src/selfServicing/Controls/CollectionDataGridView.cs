using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Data.LLBLGen.SelfServicing.ExceptionExtensions;

namespace isr.Data.LLBLGen.SelfServicing
{
    /// <summary> Extends the data grid view to save LLBLGEN Collections. </summary>
    /// <remarks> (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-08-07, 2.2.4602. </para></remarks>
    public class CollectionDataGridView : DataGridView
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes a new instance of the <see cref="CollectionDataGridView" /> class. </summary>
        public CollectionDataGridView() : base()
        {
            RowPostPaint += this.CollectionDataGridView_RowPostPaint;
            this.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.LightGreen;
            this.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            this.BorderStyle = BorderStyle.Fixed3D;
            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EnableHeadersVisualStyles = true;
            this.MultiSelect = false;
            this.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                          <c>False</c> to release only unmanaged resources when called from the
        ///                          runtime finalize. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.RemoveContentsChangedEvent( ContentsChanged );
                    this.RemoveDeleteRequestedEvent( DeleteRequested );
                    this.RemoveEditableChangedEvent( EditableChanged );
                    this.RemoveRefreshedEvent( Refreshed );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }
        #endregion

        #region " CELL FORMATTING "

        /// <summary> Work around to a problem with the data grid view failure to handle the format
        /// provider. http://stackoverflow.com/questions/3627922/format-time-span-in-datagridview-column. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnCellFormatting( DataGridViewCellFormattingEventArgs e )
        {
            if ( e is null )
            {
                base.OnCellFormatting( e );
            }
            else
            {
                if ( e.CellStyle.FormatProvider is not ICustomFormatter formatter )
                {
                    base.OnCellFormatting( e );
                }
                else
                {
                    e.Value = formatter.Format( e.CellStyle.Format, e.Value, e.CellStyle.FormatProvider );
                    e.FormattingApplied = true;
                }
            }
        }

        #endregion

        #region " DATA MANAGEMENT "


        /// <summary> Gets the binding source. </summary>
        /// <value> The binding source. </value>
        public BindingSource BindingSource { get; private set; }

        /// <summary> Gets or sets the data source that the
        /// <see cref="T:System.Windows.Forms.DataGridView" /> is displaying data for. </summary>
        /// <value> The object that contains data for the
        /// <see cref="T:System.Windows.Forms.DataGridView" /> to display. </value>
        public new object DataSource
        {
            get => this.BindingSource?.DataSource;

            set {
                this.SuspendLayout();
                try
                {
                    this.BindingSource = new BindingSource() { DataSource = value };
                }
                catch
                {
                    if ( this.BindingSource is object )
                    {
                        this.BindingSource.Dispose();
                        this.BindingSource = null;
                    }

                    throw;
                }

                base.DataSource = this.BindingSource;
                this.RowsAddedCount = 0;
                this.RowsRemovedCount = 0;
                if ( !this.DesignMode )
                {
                    this.CollectionInternal = value as SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection;
                    if ( this.CollectionInternal is object )
                    {
                        this.RemovedEntitiesInternal = this.CollectionInternal.RemovedEntitiesTracker;
                    }

                    if ( this.AutoGenerateColumns )
                    {
                        while ( this.Columns is not object || this.Columns.Count <= 0 )
                            Application.DoEvents();
                    }

                    this.OnContentsChanged( EventArgs.Empty );
                }

                this.ResumeLayout( false );
            }
        }

        #endregion

        #region " COLLECTION MANAGEMENT "

        /// <summary> Occurs when the
        /// <see cref="E:System.Windows.Forms.DataGridView.AllowUserToAddRowsChanged" /> event. </summary>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnAllowUserToAddRowsChanged( EventArgs e )
        {
            base.OnAllowUserToAddRowsChanged( e );
            this.UpdateCollectionBinding();
        }

        /// <summary> Occurs when the
        /// <see cref="E:System.Windows.Forms.DataGridView.AllowUserToDeleteRowsChanged" /> event. </summary>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnAllowUserToDeleteRowsChanged( EventArgs e )
        {
            base.OnAllowUserToDeleteRowsChanged( e );
            // The user can delete rows if the SelectionMode property is set to FullRowSelect 
            // or RowHeaderSelect and the MultiSelect property is set to true. 
            // If the DataGridView is bound to data, the BindingList.AllowRemove property of the data source must also be set to true.
            if ( this.AllowUserToDeleteRows )
            {
                this.MultiSelect = true;
                this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            }
            else
            {
                this.MultiSelect = false;
                this.SelectionMode = DataGridViewSelectionMode.CellSelect;
            }

            this.UpdateCollectionBinding();
        }

        /// <summary> Occurs when the <see cref="E:System.Windows.Forms.DataGridView.ReadOnlyChanged" />
        /// event. </summary>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnReadOnlyChanged( EventArgs e )
        {
            base.OnReadOnlyChanged( e );
            this.IsEditable = !this.ReadOnly;
        }

        /// <summary> Occurs when [editable changed]. </summary>
        public event EventHandler<EventArgs> EditableChanged;

        /// <summary> Removes editable changed event. </summary>
        /// <param name="value"> The value. </param>
        private void RemoveEditableChangedEvent( EventHandler<EventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    EditableChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Occurs when the <see cref="E:EditableChanged" /> event. </summary>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected virtual void OnEditableChanged( EventArgs e )
        {
            var evt = EditableChanged;
            evt?.Invoke( this, e );
            this.UpdateCollectionBinding();
            this.EditMode = this.IsEditable ? DataGridViewEditMode.EditOnKeystroke : DataGridViewEditMode.EditProgrammatically;

            this.RowHeadersVisible = this.IsEditable;
            this.ShowEditingIcon = this.IsEditable;
        }

        private bool _IsEditable;

        /// <summary> Allows editing. </summary>
        /// <value> The is editable. </value>
        [Category( "Behavior" )]
        [Description( "Allows editing." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool IsEditable
        {
            get => this._IsEditable;

            set {
                this._IsEditable = value;
                this.OnEditableChanged( EventArgs.Empty );
            }
        }

        /// <summary> Updates the collection binding. Sets editing, deletion and addition capabilities. </summary>
        private void UpdateCollectionBinding()
        {
            if ( !this.DesignMode && this.CollectionInternal is object )
            {
                this.CollectionInternal.AllowRemove = this.AllowUserToDeleteRows;
                this.CollectionInternal.AllowNew = this.AllowUserToAddRows;
                this.CollectionInternal.AllowEdit = !this.ReadOnly;
            }
        }

        private SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection _CollectionInternal;

        private SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection CollectionInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._CollectionInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._CollectionInternal != null )
                {
                    this._CollectionInternal.ListChanged -= this.HandleListChanged;
                }

                this._CollectionInternal = value;
                if ( this._CollectionInternal != null )
                {
                    this._CollectionInternal.ListChanged += this.HandleListChanged;
                }
            }
        }

        /// <summary> Gets the collection. </summary>
        /// <returns> The collection. </returns>
        public SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection Collection()
        {
            return this.CollectionInternal;
        }

        /// <summary> Assigns collection. </summary>
        /// <remarks> Using a collection property, even if hidden, causes the container form to fail
        /// opening in design mode. </remarks>
        /// <param name="value"> The value. </param>
        public void AssignCollection( SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection value )
        {
            this.CollectionInternal = value;
            this.UpdateCollectionBinding();
        }

        private SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection _RemovedEntitiesInternal;

        private SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection RemovedEntitiesInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._RemovedEntitiesInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._RemovedEntitiesInternal != null )
                {
                    this._RemovedEntitiesInternal.ListChanged -= this.HandleListChanged;
                }

                this._RemovedEntitiesInternal = value;
                if ( this._RemovedEntitiesInternal != null )
                {
                    this._RemovedEntitiesInternal.ListChanged += this.HandleListChanged;
                }
            }
        }
        /// <summary> Gets the removed entities. </summary>
        /// <returns> The removed entities. </returns>
        public SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection RemovedEntities()
        {
            return this.RemovedEntitiesInternal;
        }

        /// <summary> Assigns the removed entities and tracker. </summary>
        /// <param name="value"> The collection holding the removed entities. </param>
        public void AssignRemovedEntities( SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection value )
        {
            this.RemovedEntitiesInternal = value;
            if ( value is object )
            {
                this.CollectionInternal.RemovedEntitiesTracker = this.RemovedEntitiesInternal;
            }
        }

        /// <summary> Gets a value indicating whether this instance is dirty. </summary>
        /// <value> The is dirty. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool IsDirty => this.RowsAddedCount > 0 || this.RowsRemovedCount > 0 || this.CollectionInternal is object && (this.CollectionInternal.ContainsDirtyContents || this.CollectionInternal.RemovedEntitiesTracker is object && this.CollectionInternal.RemovedEntitiesTracker.Count > 0);

        /// <summary> Handles the ListChanged eventS of the edited or deleted collection. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.ComponentModel.ListChangedEventArgs" /> instance
        /// containing the event data. </param>
        private void HandleListChanged( object sender, ListChangedEventArgs e )
        {
            this.OnContentsChanged( e );
        }

        #endregion

        #region " EDIT MANAGEMENT "

        /// <summary> Occurs when a Print display is requested. </summary>
        public event EventHandler<DataGridViewRowCancelEventArgs> DeleteRequested;


        /// <summary> Removes delete requested event. </summary>
        /// <param name="value"> The value. </param>
        private void RemoveDeleteRequestedEvent( EventHandler<DataGridViewRowCancelEventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    DeleteRequested -= ( EventHandler<DataGridViewRowCancelEventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Print requested. </summary>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void OnDeleteRequested( DataGridViewRowCancelEventArgs e )
        {
            var evt = DeleteRequested;
            evt?.Invoke( this, e );
        }

        /// <summary> Handles the UserDeletingRow event of the EntityCollectionGrid control. Allows the
        /// user to cancel the deletion. </summary>
        /// <param name="e"> The <see cref="System.Windows.Forms.DataGridViewRowCancelEventArgs" />
        /// instance containing the event data. </param>
        protected override void OnUserDeletingRow( DataGridViewRowCancelEventArgs e )
        {
            if ( e is object && !e.Row.IsNewRow )
            {
                this.OnDeleteRequested( e );
                if ( !e.Cancel )
                {
                    var response = MessageBox.Show( "Are you sure you want to delete this row?", "Delete row?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly );
                    if ( response == DialogResult.No )
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        /// <summary> Gets or sets the row added count. </summary>
        /// <value> The row added count. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int RowsAddedCount { get; set; }

        /// <summary> Gets or sets the row deleted count. </summary>
        /// <value> The row deleted count. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int RowsRemovedCount { get; set; }

        /// <summary> Saves the changes. Returns the total count of the items saved and removed. </summary>
        /// <returns> A list of. </returns>
        public int SaveChanges()
        {
            int itemsRemoved = 0;
            if ( this.CollectionInternal.RemovedEntitiesTracker is object && this.CollectionInternal.RemovedEntitiesTracker.Count > 0 )
            {
                itemsRemoved = this.CollectionInternal.RemovedEntitiesTracker.DeleteMulti();
            }

            int itemsSaved = 0;
            if ( this.CollectionInternal.ContainsDirtyContents )
            {
                itemsSaved = this.CollectionInternal.SaveMulti();
            }

            return itemsSaved + itemsRemoved;
        }

        #endregion

        #region " DISPLAY ORDER MANAGEMENT "

        private List<KeyValuePair<string, int>> _ColumnDisplayOrder;

        /// <summary> Saves the column order. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="order"> The order. </param>
        public void SaveColumnDisplayOrder( List<KeyValuePair<string, int>> order )
        {
            if ( order is null )
                throw new ArgumentNullException( nameof( order ) );
            // create a list of columns ordered by column index.
            var sl = new SortedList<int, string>();
            foreach ( KeyValuePair<string, int> kvp in order )
                sl.Add( kvp.Value, kvp.Key );
            _ = sl.Reverse();
            this._ColumnDisplayOrder = new List<KeyValuePair<string, int>>();
            foreach ( KeyValuePair<int, string> kvp in sl )
                this._ColumnDisplayOrder.Add( new KeyValuePair<string, int>( kvp.Value, kvp.Key ) );
        }

        /// <summary> Saves the column order. </summary>
        public void SaveColumnDisplayOrder()
        {
            // create a list of columns ordered by column index. 
            var sl = new SortedList<int, string>();
            foreach ( DataGridViewColumn col in this.Columns )
            {
                if ( col.Visible )
                {
                    sl.Add( col.DisplayIndex, col.Name );
                }
            }

            _ = sl.Reverse();
            this._ColumnDisplayOrder = new List<KeyValuePair<string, int>>();
            foreach ( KeyValuePair<int, string> kvp in sl )
                this._ColumnDisplayOrder.Add( new KeyValuePair<string, int>( kvp.Value, kvp.Key ) );
        }

        /// <summary> Determines whether the <see cref="T:System.Windows.Forms.DataGridView">grid</see> column order mismatches
        /// the saved order. </summary>
        /// <returns> <c>True</c> if column order mismatches; otherwise, <c>False</c>. </returns>
        public bool IsColumnDisplayOrderMismatch()
        {
            bool columnOrderMismatch = false;
            if ( this._ColumnDisplayOrder is null )
            {
                return columnOrderMismatch;
            }
            else
            {
                foreach ( DataGridViewColumn col in this.Columns )
                {
                    if ( col.Visible )
                    {
                        if ( !this._ColumnDisplayOrder.Contains( new KeyValuePair<string, int>( col.Name, col.DisplayIndex ) ) )
                        {
                            columnOrderMismatch = true;
                            break;
                        }
                    }
                }
            }

            return columnOrderMismatch;
        }

        /// <summary> Determines whether [is save column display order required]. </summary>
        /// <returns> <c>True</c> if [is save column display order required]; otherwise, <c>False</c>. </returns>
        public bool IsSaveColumnDisplayOrderRequired()
        {
            return this._ColumnDisplayOrder is null || this._ColumnDisplayOrder.Count == 0;
        }

        /// <summary> Updates the column order. </summary>
        public void UpdateColumnDisplayOrder()
        {
            if ( this._ColumnDisplayOrder is null )
            {
                this.Enabled = false;
                this.Visible = false;
                this.Invalidate();
                this.Visible = true;
                this.Enabled = true;
            }
            else if ( this.IsColumnDisplayOrderMismatch() )
            {
                foreach ( KeyValuePair<string, int> kvp in this._ColumnDisplayOrder )
                    this.Columns[kvp.Key].DisplayIndex = kvp.Value;
            }
        }

        #endregion

        #region " DATA ERROR "

        /// <summary> Ignores editing errors. </summary>
        /// <value> The ignore editing errors. </value>
        [Category( "Behavior" )]
        [Description( "Ignores editing errors." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( true )]
        public bool IgnoreEditingErrors { get; set; }

        /// <summary> Gets or sets a value indicating whether to suppress data error. </summary>
        /// <value> <c>True</c> if suppressing data error; otherwise, <c>False</c>. </value>
        [Category( "Behavior" )]
        [Description( "Suppresses all data errors." )]
        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        [DefaultValue( false )]
        public bool SuppressDataError { get; set; }

        /// <summary> Ignores the data error. </summary>
        /// <remarks> Uses the collection to determine if the collection is in edit mode. </remarks>
        /// <param name="sender">     The sender. </param>
        /// <param name="e">          The <see cref="System.Windows.Forms.DataGridViewDataErrorEventArgs"/>
        /// instance containing the event data. </param>
        /// <param name="collection"> The collection. </param>
        /// <returns> <c>True</c> if editing error can be ignored, <c>False</c> otherwise. </returns>
        public static bool IgnoreDataError( object sender, DataGridViewDataErrorEventArgs e, SD.LLBLGen.Pro.ORMSupportClasses.IEntityCollection collection )
        {
            if ( e is null || sender is null )
            {
                return true;
            }
            else
            {
                if ( sender is not DataGridView grid )
                {
                    return true;
                }
                else if ( collection is object && (collection.ContainsDirtyContents || collection.RemovedEntitiesTracker is object && collection.RemovedEntitiesTracker.Count > 0) )
                {
                    return true;
                }
                else if ( grid.CurrentCell is null || grid.CurrentRow is null )
                {
                    return true;
                }
                else if ( grid.CurrentCell.IsInEditMode || grid.CurrentRow.IsNewRow || grid.IsCurrentCellInEditMode || grid.IsCurrentRowDirty )
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary> Builds the data error. </summary>
        /// <param name="grid"> The grid. </param>
        /// <param name="e">    The <see cref="System.Windows.Forms.DataGridViewDataErrorEventArgs"/>
        /// instance containing the event data. </param>
        /// <returns> System.String. </returns>
        public static string BuildDataError( DataGridView grid, DataGridViewDataErrorEventArgs e )
        {
            // prevent error reporting when adding a new row or editing a cell
            if ( grid is null || e is null || e.Exception is null || grid.CurrentCell is null || grid.CurrentRow is null )
            {
                return string.Empty;
            }
            else
            {
                string cellValue = "nothing";
                if ( grid.CurrentCell.Value is object )
                {
                    cellValue = grid.CurrentCell.Value.ToString();
                }

                return $"Data error occurred at Grid {grid.Name}(R{e.RowIndex},C{e.ColumnIndex}):{grid.Columns[e.ColumnIndex].Name}. Cell value is '{cellValue}';. {e.Exception.ToFullBlownString()}";
            }
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.DataGridView.DataError" /> event.
        /// Ignores error if editing. </summary>
        /// <remarks> Allows suppression of data error. This became necessary as we are unable to prevent
        /// data error when changing the data source of a data grid with combo boxes. </remarks>
        /// <param name="displayErrorDialogIfNoHandler"> true to display an error dialog box if there is
        /// no handler for the <see cref="E:System.Windows.Forms.DataGridView.DataError" /> event. </param>
        /// <param name="e">                             A <see cref="T:System.Windows.Forms.DataGridViewDataErrorEventArgs" /> 
        ///                                              that contains the event data. </param>
        protected override void OnDataError( bool displayErrorDialogIfNoHandler, DataGridViewDataErrorEventArgs e )
        {
            if ( this.IgnoreEditingErrors )
            {
                if ( this.IsCurrentRowDirty )
                    return;
                if ( this.IsDirty )
                    return;
                if ( this.CurrentRow is object && this.CurrentRow.IsNewRow )
                    return;
                if ( this.IsCurrentCellInEditMode )
                    return;
            }

            if ( !this.SuppressDataError )
            {
                base.OnDataError( displayErrorDialogIfNoHandler, e );
            }
        }

        #endregion

        #region " GRID EVENTS "

        /// <summary> Occurs when a refresh display is requested. </summary>
        public event EventHandler<EventArgs> Refreshed;

        /// <summary> Removes refreshed event. </summary>
        /// <param name="value"> The value. </param>
        private void RemoveRefreshedEvent( EventHandler<EventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    Refreshed -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the <see cref="E:ContentsChanged" /> event. </summary>
        public void OnRefreshed()
        {
            this.OnRefreshed( EventArgs.Empty );
        }

        /// <summary> Raises the <see cref="E:ContentsChanged" /> event. </summary>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void OnRefreshed( EventArgs e )
        {
            var evt = Refreshed;
            evt?.Invoke( this, e );
        }

        /// <summary> Occurs when after contents changed. </summary>
        public event EventHandler<EventArgs> ContentsChanged;

        /// <summary> Removes contents changed event. </summary>
        /// <param name="value"> The value. </param>
        private void RemoveContentsChangedEvent( EventHandler<EventArgs> value )
        {
            if ( value is null )
                return;
            foreach ( Delegate d in value.GetInvocationList() )
            {
                try
                {
                    ContentsChanged -= ( EventHandler<EventArgs> ) d;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }
        }

        /// <summary> Raises the <see cref="E:ContentsChanged" /> event. </summary>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void OnContentsChanged( EventArgs e )
        {
            var evt = ContentsChanged;
            evt?.Invoke( this, e );
        }

        /// <summary> Occurs with the <see cref="E:System.Windows.Forms.DataGridView.UserAddedRow" />
        /// event. Updates the number of added rows. </summary>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.DataGridViewRowEventArgs" /> that
        /// contains the event data. </param>
        protected override void OnUserAddedRow( DataGridViewRowEventArgs e )
        {
            base.OnUserAddedRow( e );
            if ( this.CollectionInternal is null || this.CollectionInternal.Count == 0 )
            {
                this.RowsAddedCount += 1;
            }
        }

        /// <summary> Occurs with the <see cref="E:System.Windows.Forms.DataGridView.UserDeletedRow" />
        /// event. Updates the number of removed rows. </summary>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.DataGridViewRowEventArgs" /> that
        /// contains the event data. </param>
        protected override void OnUserDeletedRow( DataGridViewRowEventArgs e )
        {
            base.OnUserDeletedRow( e );
            if ( this.CollectionInternal is null || this.CollectionInternal.Count == 0 )
            {
                this.RowsRemovedCount += 1;
            }
        }

        /// <summary> Gets or sets the sentinel indicating if row numbers will show. </summary>
        /// <value> The show row number. </value>
        /// <remarks> Requires </remarks>
        public bool ShowRowNumbers { get; set; }

        /// <summary> Collection data grid view row post paint. </summary>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Data grid view row post paint event information. </param>
        private void CollectionDataGridView_RowPostPaint( object sender, DataGridViewRowPostPaintEventArgs e )
        {
            if ( this.ShowRowNumbers && this.RowHeadersVisible )
            {
                using var b = new System.Drawing.SolidBrush( this.RowHeadersDefaultCellStyle.ForeColor );
                e.Graphics.DrawString( (e.RowIndex + 1).ToString(), e.InheritedRowStyle.Font, b, e.RowBounds.Location.X + 10, e.RowBounds.Location.Y + 4 );
            }
        }

        #endregion

    }
}
