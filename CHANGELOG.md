# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [4.1.8075] - 2022-02-09
* Convert to C#.

## [4.1.8070] - 2022-02-04
* Targeting Visual Studio 2022, C# 10 and .NET 6.0.
* Update NuGet packages.
* Remove unused references. 
* Update build version.
* Display version file when updating build version.
* Uses LLBLGEN NuGet package.

## [4.1.6667] - 2018-04-03
* 2018 release.

## [4.0.5866] - 2016-01-23/16
* Updates to using the new core pith library replacing trace callbacks with trace talkers and listeners. Fixes missing notification of entity property changed event.

## [3.1.5393] - 2014-10-07/14
* Adds transactions.

## [3.1.5163] - 2014-02-19/14
* Uses local sync and async safe event handlers. Removes primary key change method and auto fetch. Adds sentinel for entity fetching.

## [3.0.5151] - 2014-02-07/14
* Removes the refresh method. Raises Entity Saved only after save.

## [3.0.5150] - 2014-02-06/14
* Fixes stack overflow on Entity Name property and sets it as read only.

## [3.0.5145] - 2014-02-01/14
* Uses the trace property change publisher. Added Entity Core to the Entity Publisher Base and moved elements from the sub class to the Entity Publisher Base.

## [3.0.5126] - 2014-01-13/14
* Tagged as 2014.

## [3.0.5030] - 2013-10-09/13
* Uses new Core libraries.

## [2.2.4716] - 2012-11-29/12
* Removes refetch from base class requiring overrides on the inheriting classes to as to ensure refetch uses the prefetch paths.

## [2.2.4710] - 2012-11-23/12
* Removes .VB tags from assemblies.

## [2.2.4602] - 2012-08-07/12
* Adds Fetch Related Entities.

## [2.2.4588] - 2012-07-24/12
* Adds Refetch to the entity classes.

## [2.2.4538] - 2012-06-04/12
* Adds support for LLBLGEN 3.5.

## [2.2.4498] - 2012-04-17/12
* Implements code analysis rules for .NET 4.**

## [2.2.4300] - 2011-10-10/11
* Separated from the Publisher libraries.

## [2.2.4232] - 2011-08-03/11
* Standardize code elements and documentation.

## [2.1.4213] - 2011-07-15/11
* Simplifies the assembly information.

## [2.1.4167] - 2011-05-30/11
* Fixes bug in restoring auto fetch.

## [2.1.4163] - 2011-05-26/11
* Disables auto fetch while notifying of entity changes.

## [2.1.4157] - 2011-05-20/11
* Upgrades to LLBLGEN 3.1.

## [2.0.4102] - 2011-03-26/11
* Adds New Entity to clear the entity.

## [2.0.3997] - 2010-12-11/10
* Renames all classes to use the LLBLGEN notations for designating Entity2 as Adaptor entities and Entity as Self-Servicing Entities. Adds support for Self-Servicing entities.

## [1.0.3989] - 2010-12-03/10
* Creates a new context for a new entity.

## [1.0.3975] - 2010-11-19/10
* Created.

\(C\) 2010 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
[8075] - end branch vb2cs
```
[4.1.8075]: https://bitbucket.org/davidhary/vs.data.llblgen.oublishers.git
