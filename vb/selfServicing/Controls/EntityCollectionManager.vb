Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Data.LLBLGen.SelfServicing.ExceptionExtensions
''' <summary> Displays an entity collection. </summary>
''' <remarks> (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-01-27, 1.0.4409.x. </para></remarks>
Public Class EntityCollectionManager
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTORS"

    ''' <summary> Initializes a new instance of the <see cref="EntityCollectionManager" /> class. </summary>
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' hide controls
        Me.ActionsEnabled = False
        Me.ActionsVisible = False
        Me.PrintingEnabled = False
        Me.PrintingVisible = False
        Me.ExportEnabled = False
        Me.ExportVisible = False
        Me.SavingEnabled = False
        Me.SaveVisible = False
        Me.RefreshingEnabled = False
        Me.RefreshingVisible = False
        Me.EntityCollectionTitle = String.Empty
        Me.BottomStatusMessage = String.Empty
        Me.BottomProgressBar.Visible = False

        With Me._EntityCollectionGrid
            .AutoGenerateColumns = False
            .AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.LightGreen
            .AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.None
            .BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            .ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
            .EnableHeadersVisualStyles = True
            .MultiSelect = False
            .RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised
        End With
        Me.BottomStatusMessage = String.Empty

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources when called from the
    '''                          runtime finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.RemoveActionRequestedEvent(Me.ActionRequestedEvent)
                Me.RemoveContentChangedEvent(Me.ContentChangedEvent)
                Me.RemoveDataErrorOccurredEvent(Me.DataErrorOccurredEvent)
                Me.RemoveDeleteRequestedEvent(Me.DeleteRequestedEvent)
                Me.RemoveEntityCollectionSavedEvent(Me.EntityCollectionSavedEvent)
                Me.RemoveEntityCollectionUpdateRequestedEvent(Me.EntityCollectionUpdateRequestedEvent)
                Me.RemoveExportRequestedEvent(Me.ExportRequestedEvent)
                Me.RemoveGridVisibleChangedEvent(Me.GridVisibleChangedEvent)
                Me.RemoveRefreshRequestedEvent(Me.RefreshRequestedEvent)
                Me.RemovePrintRequestedEvent(Me.PrintRequestedEvent)
                If components IsNot Nothing Then components.Dispose() : components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " TITLES "

    ''' <summary> Gets or set the name of the information displayed in the table. </summary>
    ''' <value> The entity collection title. </value>
    <Category("Appearance"), Description("The title of the data grid."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue("ENTITY COLLECTION TITLE")>
    Public Property EntityCollectionTitle As String
        Get
            Return Me._TitleLabel.Text
        End Get
        Set(value As String)
            EntityCollectionManager.SafeTextSetter(Me._TitleLabel, value)
            If String.IsNullOrWhiteSpace(value) Then
                Me._EntityCollectionGrid.Name = Me.Name
                Me._TopToolStrip.Visible = False
            Else
                Me._TopToolStrip.Visible = True
                Me._EntityCollectionGrid.Name = value
            End If
        End Set
    End Property

#End Region

#Region " USER CONTROL "

    ''' <summary> Called when visible. Updates the controls and fires the
    ''' <see cref="GridVisibleChanged">Grid Visible Changed</see> event. </summary>
    Public Sub OnVisible()
        If _EntityCollectionGrid.IsCurrentCellInEditMode Then Return
        ' showControls()
        Dim evt As EventHandler(Of System.EventArgs) = Me.GridVisibleChangedEvent
        evt?.Invoke(Me, System.EventArgs.Empty)
    End Sub

    ''' <summary> Handles the VisibleChanged event of the EntityCollectionManager control. Updates the
    ''' control if visible. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub EntityCollectionManager_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        If Me.Visible Then
            OnVisible()
        End If
    End Sub

    ''' <summary> Shows the controls. This hides the control per the relevant settings. </summary>
    Private Sub ShowControls()
        Me.AddingRowsEnabled = Me.AddingRowsEnabled
        Me.DeletingRowsEnabled = Me.DeletingRowsEnabled
        Me.EditingEnabled = Me.EditingEnabled
        Me.PrintingEnabled = Me._EntityCollectionGrid.RowCount > 0
        ' refreshing will be enabled on the first display and stay so unless disabled externally.
        Me.RefreshingEnabled = Me.RefreshingEnabled OrElse Me._EntityCollectionGrid.RowCount > 0
        Me.ExportEnabled = Me.ExportEnabled OrElse Me._EntityCollectionGrid.RowCount > 0
        Me.ActionsEnabled = Me.ActionsVisible AndAlso Me._EntityCollectionGrid.RowCount > 0
        Me.SavingEnabled = Me.SavingEnabled OrElse (Me.EditingEnabled AndAlso Me._EntityCollectionGrid.IsDirty)
        Dim anyVisible As Boolean = False
        For Each c As Control In Me._BottomToolStrip.Controls
            If c.Visible Then
                anyVisible = True
                Exit For
            End If
        Next
        Me._BottomToolStrip.Visible = anyVisible
    End Sub

#End Region

#Region " CONTROLS "

    ''' <summary> Enables or disables adding rows. </summary>
    ''' <value> The adding rows enabled sentinel. </value>
    <Category("Behavior"), Description("Enables or disables adding rows."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property AddingRowsEnabled As Boolean
        Get
            Return Me._EntityCollectionGrid.AllowUserToAddRows
        End Get
        Set(value As Boolean)
            Me._EntityCollectionGrid.AllowUserToAddRows = value
            Me._EntityCollectionGrid.RowHeadersVisible = Me.AddingRowsEnabled OrElse Me.EditingEnabled OrElse Me.DeletingRowsEnabled
        End Set
    End Property

    ''' <summary> Allows deleting rows. </summary>
    ''' <value> The deleting rows enabled sentinel. </value>
    <Category("Behavior"), Description("Enables or disables deleting rows."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property DeletingRowsEnabled As Boolean
        Get
            Return Me._EntityCollectionGrid.AllowUserToDeleteRows
        End Get
        Set(value As Boolean)
            Me._EntityCollectionGrid.AllowUserToDeleteRows = value
            Me._EntityCollectionGrid.RowHeadersVisible = Me.AddingRowsEnabled OrElse Me.EditingEnabled OrElse Me.DeletingRowsEnabled
        End Set
    End Property

    ''' <summary> Enables or disables editing. </summary>
    ''' <value> The editing enabled sentinel. </value>
    <Category("Behavior"), Description("Enables or disables editing."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property EditingEnabled As Boolean
        Get
            Return Me._EntityCollectionGrid.IsEditable
        End Get
        Set(value As Boolean)
            Me._EntityCollectionGrid.IsEditable = value
            Me._EntityCollectionGrid.RowHeadersVisible = Me.AddingRowsEnabled OrElse Me.EditingEnabled OrElse Me.DeletingRowsEnabled
            Me.SavingEnabled = value
        End Set
    End Property

    ''' <summary> Enables or disables Refreshing. </summary>
    ''' <value> The Refreshing enabled sentinel. </value>
    ''' <remarks> Refreshing is enabled on the first display in <see cref="showControls"/> and is latched until disabled externally. </remarks>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property RefreshingEnabled As Boolean
        Get
            Return Me._RefreshButton.Enabled
        End Get
        Set(value As Boolean)
            If Not value.Equals(Me.RefreshingEnabled) Then
                Me._RefreshButton.Enabled = value
            End If
        End Set
    End Property

    ''' <summary> Shows or hides the refresh button. </summary>
    ''' <value> The Refreshing Visible sentinel. </value>
    ''' <remarks> Refreshing is Visible on the first display in <see cref="showControls"/> and is latched until disabled externally. </remarks>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property RefreshingVisible As Boolean
        Get
            Return Me._RefreshButton.Visible
        End Get
        Set(value As Boolean)
            If Not value.Equals(Me.RefreshingVisible) Then
                Me._RefreshButton.Visible = value
            End If
        End Set
    End Property

    ''' <summary> Enables or disables Saving. </summary>
    ''' <value> The Saving enabled sentinel. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property SavingEnabled As Boolean
        Get
            Return Me._SaveButton.Enabled
        End Get
        Set(value As Boolean)
            If Not value.Equals(Me.SavingEnabled) Then
                Me._SaveButton.Enabled = value
            End If
        End Set
    End Property

    ''' <summary> Enables or disables printing. </summary>
    ''' <value> The printing enabled sentinel. </value>
    <Category("Behavior"), Description("Enables or disables printing."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property PrintingEnabled As Boolean
        Get
            Return Me._PrintButton.Enabled
        End Get
        Set(value As Boolean)
            If Not value AndAlso Not value.Equals(Me.PrintingEnabled) Then
                ' disabled
                Me._PrintButton.Enabled = value
                Me._PrintButtonSeparator.Enabled = value
            End If
        End Set
    End Property

    ''' <summary> Shows or hides printing. </summary>
    ''' <value> The printing Visible sentinel. </value>
    <Category("Behavior"), Description("Shows or hides printing."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property PrintingVisible As Boolean
        Get
            Return Me._PrintButton.Visible
        End Get
        Set(value As Boolean)
            If Not value AndAlso Not value.Equals(Me.PrintingVisible) Then
                ' disabled
                Me._PrintButton.Visible = value
                Me._PrintButtonSeparator.Visible = value
            End If
        End Set
    End Property

    ''' <summary> Updates the column order. </summary>
    Public Sub UpdateColumnDisplayOrder()
        Me._EntityCollectionGrid.UpdateColumnDisplayOrder()
    End Sub

    ''' <summary> Gets the entity collection grid. </summary>
    ''' <value> The entity collection grid. </value>
    Public ReadOnly Property EntityCollectionGrid As isr.Data.LLBLGen.SelfServicing.CollectionDataGridView
        Get
            Return Me._EntityCollectionGrid
        End Get
    End Property

    ''' <summary> Gets the bottom progress bar. </summary>
    ''' <value> The bottom progress bar. </value>
    Public ReadOnly Property BottomProgressBar As System.Windows.Forms.ToolStripProgressBar
        Get
            Return Me._BottomProgressBar
        End Get
    End Property

#End Region

#Region " EVENTS "

    ''' <summary> Occurs after the grid visibility changed. Used to update the columns. </summary>
    Public Event GridVisibleChanged As EventHandler(Of System.EventArgs)

    ''' <summary> RemoveS grid visible changed event. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveGridVisibleChangedEvent(ByVal value As EventHandler(Of EventArgs))
        If value Is Nothing Then Return
        For Each d As [Delegate] In value.GetInvocationList
            Try
                RemoveHandler Me.GridVisibleChanged, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Occurs when a display update is requested. </summary>
    Public Event EntityCollectionUpdateRequested As EventHandler(Of System.EventArgs)

    ''' <summary> Removes entity collection update requested event. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveEntityCollectionUpdateRequestedEvent(ByVal value As EventHandler(Of EventArgs))
        If value Is Nothing Then Return
        For Each d As [Delegate] In value.GetInvocationList
            Try
                RemoveHandler Me.EntityCollectionUpdateRequested, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Entity collection update requested. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub OnEntityCollectionUpdateRequested(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.EntityCollectionUpdateRequestedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Occurs after data was saved. </summary>
    Public Event EntityCollectionSaved As EventHandler(Of System.EventArgs)

    ''' <summary> Removes entity collection saved event. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveEntityCollectionSavedEvent(ByVal value As EventHandler(Of EventArgs))
        If value Is Nothing Then Return
        For Each d As [Delegate] In value.GetInvocationList
            Try
                RemoveHandler Me.EntityCollectionSaved, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the <see cref="E:EntityCollectionChanged" /> event. </summary>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub OnEntityCollectionSaved(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.EntityCollectionSavedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Occurs when a refresh display is requested. </summary>
    Public Event RefreshRequested As EventHandler(Of System.EventArgs)

    ''' <summary> Removes the refresh requested event described by value. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveRefreshRequestedEvent(ByVal value As EventHandler(Of EventArgs))
        If value Is Nothing Then Return
        For Each d As [Delegate] In value.GetInvocationList
            Try
                RemoveHandler Me.RefreshRequested, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Refresh requested. </summary>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub OnRefreshRequested(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.RefreshRequestedEvent
        evt?.Invoke(Me, e)
    End Sub

#End Region

#Region " GRID EVENTS "

    ''' <summary> Occurs when a refresh display is requested. </summary>
    Public Event ContentChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Removes content changed event 1. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveContentChangedEvent(ByVal value As EventHandler(Of EventArgs))
        If value Is Nothing Then Return
        For Each d As [Delegate] In value.GetInvocationList
            Try
                RemoveHandler Me.ContentChanged, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the <see cref="E:ContentsChanged" /> event. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub OnContentsChanged(ByVal e As System.EventArgs)
        Me.ActionsEnabled = Me._EntityCollectionGrid.RowCount > 0
        Me.PrintingEnabled = Me._EntityCollectionGrid.RowCount > 0
        Me.RefreshingEnabled = Me.RefreshingEnabled OrElse Me._EntityCollectionGrid.RowCount > 0
        Me.SavingEnabled = (Me.DeletingRowsEnabled OrElse Me.EditingEnabled) AndAlso Me._EntityCollectionGrid.IsDirty
        Dim evt As EventHandler(Of System.EventArgs) = Me.ContentChangedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Entity collection grid refreshed. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EntityCollectionGrid_Refreshed(ByVal sender As Object, ByVal e As System.EventArgs) Handles _EntityCollectionGrid.Refreshed
        Me.showControls()
    End Sub

    ''' <summary> Entity collection grid content changed. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EntityCollectionGrid_ContentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _EntityCollectionGrid.ContentsChanged
        Me.OnContentsChanged(e)
    End Sub

    ''' <summary> Occurs when [data error]. </summary>
    Public Event DataErrorOccurred As EventHandler(Of System.Windows.Forms.DataGridViewDataErrorEventArgs)

    ''' <summary> Removes the data error occurred event described by value. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveDataErrorOccurredEvent(ByVal value As EventHandler(Of System.Windows.Forms.DataGridViewDataErrorEventArgs))
        If value Is Nothing Then Return
        For Each d As [Delegate] In value.GetInvocationList
            Try
                RemoveHandler Me.DataErrorOccurred, CType(d, EventHandler(Of System.Windows.Forms.DataGridViewDataErrorEventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Handles the DataError event of the EntityCollectionGrid control. Reports the error to
    ''' the parent control. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.Windows.Forms.DataGridViewDataErrorEventArgs" />
    ''' instance containing the event data. </param>
    Private Sub EntityCollectionGrid_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles _EntityCollectionGrid.DataError
        If DataErrorOccurredEvent IsNot Nothing Then DataErrorOccurredEvent(Me, e)
    End Sub

    ''' <summary> Entity collection grid double click. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EntityCollectionGrid_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _EntityCollectionGrid.DoubleClick
        If _EntityCollectionGrid.IsCurrentCellInEditMode Then Return
        Me.RequestDisplayUpdate()
    End Sub

    ''' <summary> Handles the VisibleChanged event of the EntityCollectionGrid control. Needed to
    ''' update the columns. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub EntityCollectionGrid_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _EntityCollectionGrid.VisibleChanged
        If _EntityCollectionGrid.IsCurrentCellInEditMode Then Return
        Dim evt As EventHandler(Of System.EventArgs) = Me.GridVisibleChangedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Occurs when a delete is requested. </summary>
    Public Event DeleteRequested As EventHandler(Of System.Windows.Forms.DataGridViewRowCancelEventArgs)

    ''' <summary> Removes deleted requested event 1. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveDeleteRequestedEvent(ByVal value As EventHandler(Of System.Windows.Forms.DataGridViewRowCancelEventArgs))
        If value Is Nothing Then Return
        For Each d As [Delegate] In value.GetInvocationList
            Try
                RemoveHandler Me.DeleteRequested, CType(d, EventHandler(Of System.Windows.Forms.DataGridViewRowCancelEventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the delete requested event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnDeleteRequested(ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs)
        Dim evt As EventHandler(Of System.Windows.Forms.DataGridViewRowCancelEventArgs) = Me.DeleteRequestedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Entity collection grid user deleting row. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Data grid view row cancel event information. </param>
    Private Sub EntityCollectionGrid_UserDeletingRow(sender As Object, e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles _EntityCollectionGrid.UserDeletingRow
        Me.onDeleteRequested(e)
    End Sub

    ''' <summary> Entity collection grid user deleting row. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Data grid view row cancel event information. </param>
    Private Sub EntityCollectionGrid_DeletedRequested(sender As Object, e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles _EntityCollectionGrid.DeleteRequested
        Me.onDeleteRequested(e)
    End Sub

    ''' <summary> Occurs when a selection is changed. </summary>
    Public Event SelectionChanged As EventHandler(Of System.EventArgs)

    ''' <summary> Selection changed. </summary>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub OnSelectionChanged(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.SelectionChangedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Entity collection grid selection changed. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EntityCollectionGrid_SelectionChanged(sender As Object, e As System.EventArgs) Handles _EntityCollectionGrid.SelectionChanged
        Me.onSelectionChanged(e)
    End Sub


#End Region

#Region " REFRESH EVENTS "

    ''' <summary> Requests the display update. </summary>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function RequestDisplayUpdate() As Boolean
        Me.onEntityCollectionUpdateRequested(System.EventArgs.Empty)
        Return True
    End Function

    ''' <summary>
    ''' Handles the Click event of the _RefreshButton control.
    ''' Request a refresh of the display.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub RefreshButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _RefreshButton.Click
        ' 2014-06-23 Me._EntityCollectionGrid.DataSource = Nothing
        Me.UpdateBottomStatusMessage("Refresh of {0} requested", Me.EntityCollectionTitle)
        Me.onRefreshRequested(System.EventArgs.Empty)
        Me.BottomStatusMessage = "Refresh processed"
    End Sub

#End Region

#Region " PRINTING "


    ''' <summary> Occurs when a Print display is requested. </summary>
    Public Event PrintRequested As EventHandler(Of System.EventArgs)

    ''' <summary> Removes print requested event. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemovePrintRequestedEvent(ByVal value As EventHandler(Of System.EventArgs))
        If value Is Nothing Then Return
        For Each d As [Delegate] In value.GetInvocationList
            Try
                RemoveHandler Me.PrintRequested, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Print requested. </summary>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub OnPrintRequested(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.PrintRequestedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Request printing. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PrintButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _PrintButton.Click
        Dim activity As String = String.Empty
        Try
            Me._ErrorProvider.Clear()
            activity = $"Printing of {Me.EntityCollectionTitle} requested"
            Me.UpdateBottomStatusMessage(activity)
            Me.OnPrintRequested(System.EventArgs.Empty)
            activity = "Printing processed"
            Me.BottomStatusMessage = activity
        Catch ex As Exception
            Me.Enunciate(Me._BottomToolStrip, $"Exception occurred {activity}")
            Me.BottomStatusMessage = $"Exception occurred {activity}"
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SAVING "

    ''' <summary> Gets the sentinel indicated if the grid collection has dirty entities. </summary>
    ''' <value> <c>True</c> if the grid collection has dirty entities. </value>
    Public ReadOnly Property IsDirty As Boolean
        Get
            Return Me._EntityCollectionGrid IsNot Nothing AndAlso Me._EntityCollectionGrid.IsDirty
        End Get
    End Property

    ''' <summary> Saves changes to collection. </summary>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SaveButton.Click
        Try
            Me._ErrorProvider.Clear()
            Dim itemCount As Integer = 0
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If Me._EntityCollectionGrid.IsDirty Then
                Me.UpdateBottomStatusMessage("Saving changes in {0}...", Me.EntityCollectionTitle)
                Me.Talker?.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Saving {0};. ", Me.EntityCollectionTitle)
                itemCount = Me._EntityCollectionGrid.SaveChanges

                Me.UpdateBottomStatusMessage("Saving completed--{0} {1} items saved and/or removed.", Me.EntityCollectionTitle, itemCount)
                Me.Talker?.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId,
                                           "Saving of {0} completed; {1} items saved and/or removed;. ", Me.EntityCollectionTitle, itemCount)

            End If
            If itemCount > 0 Then
                Me.OnEntityCollectionSaved(System.EventArgs.Empty)
            End If
        Catch ex As Exception
            Me.enunciate(Me._BottomToolStrip, "Exception occurred saving")
            Me.UpdateBottomStatusMessage("Exception occurred saving/deleting {0}", Me.EntityCollectionTitle)
            Me.Talker?.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                       "Exception occurred saving/deleting '{0}';. {1}",
                                       Me.EntityCollectionTitle, ex.ToFullBlownString)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub


    ''' <summary> Shows or hides the Save button. </summary>
    ''' <value> The Action Visible sentinel. </value>
    <Category("Behavior"), Description("Shows or hides the Save button."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property SaveVisible As Boolean
        Get
            Return Me._SaveButton.Visible
        End Get
        Set(value As Boolean)
            If Not value.Equals(Me.SaveVisible) Then
                Me._SaveButton.Visible = value
                Me._SaveButtonSeparator.Visible = value
            End If
        End Set
    End Property

#End Region

#Region " ACTION "

    ''' <summary> Gets or set the name of the actions button. </summary>
    <Category("Appearance"), Description("The title of the actions button."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue("")>
    Public Property ActionsTitle As String
        Get
            Return Me._ActionsButton.Text
        End Get
        Set(value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            EntityCollectionManager.SafeTextSetter(Me._ActionsButton, value)
            If String.IsNullOrWhiteSpace(value) Then
                Me._ActionsButton.DisplayStyle = ToolStripItemDisplayStyle.Image
            Else
                Me._ActionsButton.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText
            End If
        End Set
    End Property

    ''' <summary> Enables or disables actions. </summary>
    ''' <value> The Action enabled sentinel. </value>
    <Category("Behavior"), Description("Enables or disables actions."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property ActionsEnabled As Boolean
        Get
            Return Me._ActionsButton.Enabled
        End Get
        Set(value As Boolean)
            If Not value.Equals(Me.ActionsEnabled) Then
                Me._ActionsButton.Enabled = value
                Me._ActionsButtonSeparator.Enabled = value
            End If
        End Set
    End Property

    ''' <summary> Shows or hides the actions button. </summary>
    ''' <value> The Action Visible sentinel. </value>
    <Category("Behavior"), Description("Shows or hides the actions button."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property ActionsVisible As Boolean
        Get
            Return Me._ActionsButton.Visible
        End Get
        Set(value As Boolean)
            If Not value.Equals(Me.ActionsVisible) Then
                Me._ActionsButton.Visible = value
                Me._ActionsButtonSeparator.Visible = value
            End If
        End Set
    End Property

    ''' <summary> Occurs when a Action is requested. </summary>
    Public Event ActionRequested As EventHandler(Of System.EventArgs)

    ''' <summary> Removes action requested event. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveActionRequestedEvent(ByVal value As EventHandler(Of EventArgs))
        If value Is Nothing Then Return
        For Each d As [Delegate] In value.GetInvocationList
            Try
                RemoveHandler Me.ActionRequested, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the action requested event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnActionRequested(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.ActionRequestedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Event handler. Called by _ActionsButton for click events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ActionsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ActionsButton.Click
        Me.BottomStatusMessage = "Action requested..."
        Me.onActionRequested(System.EventArgs.Empty)
        Me.SavingEnabled = Me.SavingEnabled OrElse Me.IsDirty
        Me.BottomStatusMessage = "Action processed"
    End Sub

#End Region


#Region " EXPORT "

    ''' <summary> Gets or set the name of the Export button. </summary>
    <Category("Appearance"), Description("The title of the Export button."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue("")>
    Public Property ExportTitle As String
        Get
            Return Me._ExportButton.Text
        End Get
        Set(value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Me._ExportButton IsNot Nothing AndAlso Not String.Equals(value, Me.ExportTitle) Then
                Me.ExportEnabled = Not String.IsNullOrWhiteSpace(value)
                EntityCollectionManager.SafeTextSetter(Me._ExportButton, value)
                If String.IsNullOrWhiteSpace(value) Then
                    Me._ExportButton.DisplayStyle = ToolStripItemDisplayStyle.Image
                Else
                    Me._ExportButton.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText
                End If
            End If
        End Set
    End Property

    ''' <summary> Enables or disables Export. </summary>
    ''' <value> The Export enabled sentinel. </value>
    <Category("Behavior"), Description("Enables or disables Export."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property ExportEnabled As Boolean
        Get
            Return Me._ExportButton.Enabled
        End Get
        Set(value As Boolean)
            If Me._ExportButton IsNot Nothing AndAlso Not value.Equals(Me.ExportEnabled) Then
                Me._ExportButton.Enabled = value
                Me._ExportButtonSeparator.Enabled = value
            End If
        End Set
    End Property

    ''' <summary> Shows or hides the Export button. </summary>
    ''' <value> The Action Visible sentinel. </value>
    <Category("Behavior"), Description("Shows or hides the Export button."),
        Browsable(True),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
        DefaultValue(False)>
    Public Property ExportVisible As Boolean
        Get
            Return Me._ExportButton.Visible
        End Get
        Set(value As Boolean)
            If Not value.Equals(Me.ExportVisible) Then
                Me._ExportButton.Visible = value
                Me._ExportButtonSeparator.Visible = value
            End If
        End Set
    End Property

    ''' <summary> Occurs when a Export is requested. </summary>
    Public Event ExportRequested As EventHandler(Of System.EventArgs)

    ''' <summary> Removes export requested event. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RemoveExportRequestedEvent(ByVal value As EventHandler(Of EventArgs))
        If value Is Nothing Then Return
        For Each d As [Delegate] In value.GetInvocationList
            Try
                RemoveHandler Me.ExportRequested, CType(d, EventHandler(Of System.EventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the Export requested event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnExportRequested(ByVal e As System.EventArgs)
        Dim evt As EventHandler(Of System.EventArgs) = Me.ExportRequestedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Event handler. Called by _ExportButton for click events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ExportButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ExportButton.Click
        Me.UpdateBottomStatusMessage("Export of {0} requested", Me.EntityCollectionTitle)
        Me.onExportRequested(System.EventArgs.Empty)
        Me.BottomStatusMessage = "Export processed"
    End Sub

#End Region


#Region " MESSAGES "

    ''' <summary> Enunciates the specified control. </summary>
    ''' <param name="control"> The control. </param>
    ''' <param name="format">  The format. </param>
    ''' <param name="args">    The arguments. </param>
    Private Sub Enunciate(ByVal control As Control, ByVal format As String, ByVal ParamArray args As Object())
        Me._ErrorProvider.Clear()
        Me._ErrorProvider.SetError(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary> Updates the bottom status label. </summary>
    ''' <param name="format">   The format. </param>
    ''' <param name="args">     The arguments. </param>
    Public Sub UpdateBottomStatusMessage(ByVal format As String, ByVal ParamArray args() As Object)
        Me.BottomStatusMessage = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
    End Sub

    ''' <summary> Gets or sets a message describing the bottom status. </summary>
    ''' <value> A message describing the bottom status. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property BottomStatusMessage As String
        Get
            Return Me._BottomStatusLabel.Text
        End Get
        Set(value As String)
            EntityCollectionManager.SafeTextSetter(Me._BottomStatusLabel, value)
        End Set
    End Property

#End Region

#Region " SAFE SETTERS "

    ''' <summary> Safe text setter. </summary>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   The synopsis to display. </param>
    Private Shared Sub SafeTextSetter(ByVal control As Control, ByVal value As String)
        If String.IsNullOrEmpty(value) Then value = String.Empty
        If control IsNot Nothing Then
            If control.InvokeRequired Then
                control.Invoke(New Action(Of Control, String)(AddressOf EntityCollectionManager.SafeTextSetter), New Object() {control, value})
            Else
                control.Text = value
            End If
        End If
    End Sub

    ''' <summary> Safe text setter. </summary>
    ''' <param name="control"> The control. </param>
    ''' <param name="value">   The synopsis to display. </param>
    Private Shared Sub SafeTextSetter(ByVal control As ToolStripItem, ByVal value As String)
        If String.IsNullOrEmpty(value) Then value = String.Empty
        If control IsNot Nothing Then
            If control.Owner.InvokeRequired Then
                control.Owner.Invoke(New Action(Of Control, String)(AddressOf EntityCollectionManager.SafeTextSetter), New Object() {control, value})
            Else
                control.Text = value
            End If
        End If
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Identify(Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> Declared as must override so that the Trace Event Id of the caller library is used </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> Declared as must override so that the <see cref="ToFullBlownString(Exception)"/> is extended based on the caller library implementation </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region


End Class

