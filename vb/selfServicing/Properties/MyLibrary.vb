Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Data.LLBLGen.Publishers.My.ProjectTraceEventId.SelfServicing

        Public Const AssemblyTitle As String = "LLBLGen Self Servicing Library"
        Public Const AssemblyDescription As String = "LLBLGen O/R Mapping Self Servicing Library"
        Public Const AssemblyProduct As String = "LLBLGen.SelfServicing"

    End Class

End Namespace

