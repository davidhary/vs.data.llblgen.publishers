Imports isr.Data.LLBLGen.SelfServicingEntityExtensions
Imports SD.LLBLGen.Pro.ORMSupportClasses
Imports isr.Data.LLBLGen.Publishers
Imports isr.Data.LLBLGen.SelfServicing.ExceptionExtensions
''' <summary> Base class for publishing entity information. </summary>
''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2010-10-03, 1.0.3928.x. </para></remarks>
Public MustInherit Class EntityPublisherBase(Of TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase, SD.LLBLGen.Pro.ORMSupportClasses.IEntity})
    Inherits EntityPublisherBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs the class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources when called from the
    '''                          runtime finalize. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.EntityInternal = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " O/R MAPPING "

    ''' <summary> Builds prefetch path for the entity. </summary>
    ''' <returns> The <see cref="SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath">prefetch path</see> </returns>
    Protected MustOverride Function BuildPrefetchPath() As SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath

#End Region

#Region " ENTITY "

    Private WithEvents EntityInternal As TEntity

    ''' <summary> Gets a reference to the entity. </summary>
    ''' <value> The entity. </value>
    Public ReadOnly Property Entity() As TEntity
        Get
            Return Me.EntityInternal
        End Get
    End Property

#End Region

#Region " ENTITY NO PUBLISH "

    ''' <summary> Saves the active entity without publishing events. </summary>
    ''' <returns> <c>True</c> if entity was save and successfully refetched. </returns>
    Public Overrides Function SaveEntity() As Boolean

        Dim fieldNames As String = String.Join(",", Me.Entity.GetPrimaryKeyFieldNames.ToArray)
        Me.Talker?.Publish(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Saving entity; {0} '{1}'", Me.Entity, fieldNames)
        If Me.Entity.Save() Then
            ' refetch to keep entity active without raising events.
            If Me.RefetchEntity() Then
                Return True
            Else
                Me.Talker?.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                           "Failed fetching entity after saving;. '{0}' '{1}' from the database after saving.", Me.Entity, fieldNames)
                Return False
            End If
        Else
            Me.Talker?.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                       "Failed saving entity;. '{0}' '{1}' to the database.", Me.Entity, fieldNames)
            Return False
        End If
        Return False

    End Function

    ''' <summary> Sets the entity. </summary>
    ''' <param name="value"> The value. </param>
    Public Overloads Sub EntitySetter(ByVal value As TEntity)
        Me.EntityInternal = value
        MyBase.EntitySetter(value)
    End Sub

#End Region

#Region " ENTITY WITH PUBLISH "

    ''' <summary> Deletes the selected entity. </summary>
    ''' <remarks> Raises the <see cref="EntityCreated">entity created</see> and the
    ''' <see cref="EntityChanged">entity changed</see> events. </remarks>
    ''' <param name="transaction"> The transaction. </param>
    ''' <returns> <c>True</c> if entity was deleted. </returns>
    Public Overrides Function Delete(ByVal transaction As TransactionBase) As Boolean
        Dim result As Boolean = False
        If Me.Entity Is Nothing OrElse Me.Entity.IsNew Then
            result = True
        Else
            If transaction Is Nothing Then
                Throw New ArgumentNullException(NameOf(transaction))
            End If
            Try
                transaction.Add(Me.Entity)
                result = Me.Entity.Delete
                transaction.Commit()
            Catch ex As SD.LLBLGen.Pro.ORMSupportClasses.ORMQueryExecutionException
                transaction.Rollback()
                Me.Talker?.Publish(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                           "Exception occurred deleting entity;. '{0}'{1}{2}",
                                           Me.Entity, ex.QueryExecuted, ex.ToFullBlownString)
            Finally
                If result Then
                    Me.CreateEntity(True)
                    Me.OnEntityChanged()

                End If
            End Try
        End If
        Return result
    End Function

    ''' <summary> Saves the active entity. </summary>
    ''' <returns> <c>True</c> if entity was save and successfully refetched. </returns>
    Public Overrides Function Save() As Boolean

        If Me.SaveEntity() Then
            ' raise the saved and let the calling application decide if need to do entity changed and entities changed.
            Me.OnEntitySaved()
            Return True
        End If
        Return False

    End Function

#End Region

End Class


