﻿Imports SD.LLBLGen.Pro.ORMSupportClasses
''' <summary> Base class for anonymously publishing entity information for an entity that has a
''' primary key consisting of one field. </summary>
''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2010-10-03, 1.0.3928.x. </para></remarks>
Public MustInherit Class EntityPublisherBase1(Of TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase, SD.LLBLGen.Pro.ORMSupportClasses.IEntity},
                                                  TPrimaryKey)
    Inherits EntityPublisherBase(Of TEntity)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs the class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " ENTITY BASE METHOD - NO EVENTS "

    ''' <summary> Fetches an entity or clears exiting entity. </summary>
    ''' <param name="primaryKey"> Specifies the first primary key. </param>
    ''' <returns> <c>True</c> if entity exists. Otherwise, a new entity is created. </returns>
    ''' <remarks> Raises the <see cref="EntityCreated">entity created</see> event. Does not raise
    ''' the <see cref="EntityChanged">entity changed</see> event. </remarks>
    Protected MustOverride Function FetchEntity(ByVal primaryKey As TPrimaryKey) As Boolean

#End Region

#Region " ENTITY "

    ''' <summary> Fetches a new or exiting adapter using the primary key. </summary>
    ''' <param name="primaryKey"> Specifies a adapter number. </param>
    ''' <returns> <c>True</c> if entity exists. Otherwise, a new entity is created. </returns>
    Public Overridable Function Fetch(ByVal primaryKey As TPrimaryKey) As Boolean
        Me.FetchEntity(primaryKey)
        Me.OnEntityChanged()
        Return Not Me.Entity.IsNew
    End Function

    ''' <summary> Checks if an entity exists. </summary>
    ''' <remarks> Find does not trigger the entity changed event. </remarks>
    ''' <param name="primaryKey"> Specifies the primary key. </param>
    ''' <returns> <c>True</c> if entity exists. </returns>
    Public MustOverride Function Exists(ByVal primaryKey As TPrimaryKey) As Boolean

    ''' <summary> Checks if a new entity needs to be fetched. </summary>
    ''' <param name="primaryKey"> The primary key. </param>
    ''' <returns> <c>True</c> if a new entity needs to be fetched; otherwise, <c>False</c>. </returns>
    Public MustOverride Overloads Function IsFetchRequired(ByVal primaryKey As TPrimaryKey) As Boolean

#End Region

End Class
