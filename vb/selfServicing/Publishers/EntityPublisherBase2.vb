﻿Imports SD.LLBLGen.Pro.ORMSupportClasses
''' <summary> Base class for anonymously publishing entity information for an entity that has a
''' primary key consisting of two values. </summary>
''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2010-10-03, 1.0.3928.x. </para></remarks>
<CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes")>
Public MustInherit Class EntityAnonPublisherBase2(Of TEntity As {SD.LLBLGen.Pro.ORMSupportClasses.EntityBase, SD.LLBLGen.Pro.ORMSupportClasses.IEntity},
                                                      TPrimaryKey1, TPrimaryKey2)
    Inherits EntityPublisherBase(Of TEntity)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs the class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " ENTITY BASE METHOD - NO EVENTS "

    ''' <summary> Fetches an entity or clears exiting entity. </summary>
    ''' <param name="primaryKey1"> Specifies the first primary key. </param>
    ''' <param name="primaryKey2"> Specifies the second primary key. </param>
    ''' <returns> <c>True</c> if entity exists. Otherwise, a new entity is created. </returns>
    ''' <remarks> Raises the <see cref="EntityCreated">entity created</see> event. Does not raise
    ''' the <see cref="EntityChanged">entity changed</see> event. </remarks>
    Protected MustOverride Function FetchEntity(ByVal primaryKey1 As TPrimaryKey1, ByVal primaryKey2 As TPrimaryKey2) As Boolean

#End Region

#Region " ENTITY "

    ''' <summary> Fetches a new or exiting adapter using the primary key. </summary>
    ''' <param name="primaryKey1"> Specifies the first primary key. </param>
    ''' <param name="primaryKey2"> Specifies the second primary key. </param>
    ''' <returns> <c>True</c> if entity exists. Otherwise, a new entity is created. </returns>
    Public Overridable Function Fetch(ByVal primaryKey1 As TPrimaryKey1, ByVal primaryKey2 As TPrimaryKey2) As Boolean

        Me.FetchEntity(primaryKey1, primaryKey2)
        OnEntityChanged()
        Return Not Me.Entity.IsNew

    End Function

    ''' <summary> Checks if an entity exists. </summary>
    ''' <remarks> Find does not trigger the entity changed event. </remarks>
    ''' <param name="primaryKey1"> Specifies the first primary key. </param>
    ''' <param name="primaryKey2"> Specifies the second primary key. </param>
    ''' <returns> <c>True</c> if entity exists. </returns>
    Public MustOverride Function Exists(ByVal primaryKey1 As TPrimaryKey1, ByVal primaryKey2 As TPrimaryKey2) As Boolean

#End Region

End Class
