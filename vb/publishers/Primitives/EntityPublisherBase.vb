Imports System.ComponentModel
Imports SD.LLBLGen.Pro.ORMSupportClasses
Imports isr.Core
Imports isr.Data.LLBLGen.Publishers.ExceptionExtensions
''' <summary> Base class for publishing entity information. This interfaces is
''' independent of the entity type. </summary>
''' <remarks> 
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2010-10-03, 1.0.3928.x. </para></remarks>
Public MustInherit Class EntityPublisherBase
    Inherits isr.Core.Models.ViewModelTalkerBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets the is disposed. </summary>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <value> The is disposed. </value>
    Public ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Services.MyLog and optionally releases the
    ''' managed resources.
    ''' </summary>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.Talker?.Listeners.Clear()
                Me._Entity = Nothing
                Me.RemoveEntityChangedEventHandlers()
                Me.RemoveEntityCreatedEventHandlers()
                Me.RemoveEntityPropertyChangedEventHandlers()
                Me.RemoveEntitySavedEventHandlers()
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region
#End Region

#Region " ENTITY "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _Entity As SD.LLBLGen.Pro.ORMSupportClasses.IEntityCore
#Enable Warning IDE1006 ' Naming Styles
    ''' <summary> Gets or sets the entity core. </summary>
    ''' <value> The entity core. </value>
    Private ReadOnly Property Entity As SD.LLBLGen.Pro.ORMSupportClasses.IEntityCore
        Get
            Return Me._Entity
        End Get
    End Property

    ''' <summary> Sets the entity. </summary>
    ''' <param name="value"> The value. </param>
    Public Overridable Sub EntitySetter(ByVal value As SD.LLBLGen.Pro.ORMSupportClasses.IEntityCore)
        Me._Entity = value
        Me.AsyncNotifyPropertyChanged(NameOf(EntityPublisherBase.Entity))
    End Sub

    ''' <summary> Gets or sets the name of the entity. </summary>
    ''' <value> The name of the entity. </value>
    Public ReadOnly Property EntityName() As String

        Get
            Return If(Me.Entity Is Nothing, $"{Me.GetType.Name}.Entity", Me.Entity.LLBLGenProEntityName)
        End Get
    End Property

    ''' <summary> Determines whether the entity needs to be fetched. 
    '''           The entity needs to be fetched if it does not exists or is new. </summary>
    ''' <returns> <c>True</c> if entity data needs to be saved; otherwise, <c>False</c>. </returns>
    Public Function IsFetchRequired() As Boolean
        Return Me.Entity Is Nothing OrElse Me.IsEntityNew
    End Function

    ''' <summary> Determines whether the entity needs to be saved. 
    '''           The entity needs to be saved if it exists and is dirty. </summary>
    ''' <returns> <c>True</c> if entity data needs to be saved; otherwise, <c>False</c>. </returns>
    Public Overridable Function IsSaveRequired() As Boolean
        Return Me.Entity IsNot Nothing AndAlso Me.IsEntityDirty
    End Function

    ''' <summary> Determines whether an existing entity must be saved for the specified elements. Same
    ''' as <see cref="IsEntityDirty">dirty</see>. </summary>
    ''' <returns> <c>True</c> if a new entity needs to be created or saved or the existing entity must
    ''' be saved for the specified elements; otherwise, <c>False</c>. </returns>
    Public Function IsUpdateRequired() As Boolean
        Return Me.Entity IsNot Nothing AndAlso Me.Entity.IsDirty
    End Function

    ''' <summary> Returns <c>True</c> if the entity was instantiated. </summary>
    ''' <returns> <c>True</c> if the entity exists: not nothing; Otherwise, <c>False</c>. </returns>
    Public Function HasEntity() As Boolean
        Return Me._Entity IsNot Nothing
    End Function

    ''' <summary> Determines if the entity is clean: exists and not new or dirty. </summary>
    ''' <returns> <c>True</c> if the entity exists and is not new or dirty; Otherwise, <c>False</c>. </returns>
    Public Function IsEntityClean() As Boolean
        Return Me.Entity IsNot Nothing AndAlso Not (Me.IsEntityNew OrElse Me.IsEntityDirty)
    End Function

    ''' <summary> Determines if the entity is dirty: exists and dirty. </summary>
    ''' <returns> <c>True</c> if the entity exists and is dirty; Otherwise, <c>False</c>. </returns>
    Public Function IsEntityDirty() As Boolean
        Return Me.Entity IsNot Nothing AndAlso Me.Entity.IsDirty
    End Function

    ''' <summary> Determines if the entity is new: exists and new. </summary>
    ''' <returns> <c>True</c> if the entity exists and is new; Otherwise, <c>False</c>. </returns>
    Public Function IsEntityNew() As Boolean
        Return Me.Entity IsNot Nothing AndAlso Me.Entity.IsNew
    End Function

    ''' <summary> Determines whether the entity has valid entity data. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Cancel details event information. </param>
    ''' <returns>
    ''' <c>True</c> if has valid entity data for the specified entity; otherwise,
    ''' <c>False</c>.
    ''' </returns>
    Public Function IsEntityClean(ByVal e As isr.Core.ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If Me.Entity Is Nothing Then
            e.RegisterFailure("Invalid entity data;. select {0}", Me.EntityName)
        ElseIf Me.Entity.IsNew Then
            e.RegisterFailure("New entity;. Entity {0} must be saved before its data can be used.", Me.EntityName)
        ElseIf Me.Entity.IsDirty Then
            e.RegisterFailure("Entity {0} has unsaved data;. ", Me.EntityName)
        Else
        End If
        Return Not e.Failed
    End Function

#End Region

#Region " ENTITY NO PUBLISH "

    ''' <summary> Saves the active entity. </summary>
    ''' <remarks> Does not publish the <see cref="EntitySaved">entity saved</see> event. </remarks>
    ''' <returns> <c>True</c> if entity was save and successfully refetched. </returns>
    Public MustOverride Function SaveEntity() As Boolean

    ''' <summary> Refetches the active entity. Also fetches related entities. Override using a full
    ''' fetch if the entity has a <see cref="SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath">prefetch
    ''' path</see>. </summary>
    ''' <remarks> Does not publish the <see cref="EntityChanged">entity changed</see> event. </remarks>
    ''' <returns> <c>True</c> if entity was refetched. </returns>
    Public MustOverride Function RefetchEntity() As Boolean

    ''' <summary> Instantiates a new entity. </summary>
    ''' <remarks> Uses the <see cref="OnEntityChanged">entity changed event</see> to notify of the change in
    ''' entity. Use without notifications when creating a new entity before saving. </remarks>
    ''' <param name="notify"> <c>True</c> to notify. </param>
    Public MustOverride Sub CreateEntity(ByVal notify As Boolean)

#End Region

#Region " ENTITY WITH PUBLISH "

    ''' <summary> Gets or sets the is fetching sentinel. Indicates that the entity is being fetched.
    ''' Useful when using auto fetch on primary key changes. </summary>
    ''' <value> <c>True</c> if the entity is being fetched. </value>
    Public Property IsFetching As Boolean

    ''' <summary> Deletes the selected entity. </summary>
    ''' <returns> <c>True</c> if entity was deleted. </returns>
    Public MustOverride Function Delete() As Boolean

    ''' <summary> Deletes the selected entity. </summary>
    ''' <param name="transaction"> The transaction. </param>
    ''' <returns> <c>True</c> if entity was deleted. </returns>
    Public MustOverride Function Delete(ByVal transaction As TransactionBase) As Boolean

    ''' <summary> Fetches related entities. </summary>
    ''' <returns> <c>True</c> if entities were fetched. </returns>
    Public MustOverride Function FetchRelatedEntities() As Boolean

    ''' <summary> Fetches related entities. </summary>
    ''' <param name="transaction"> The transaction. </param>
    ''' <returns> <c>True</c> if entities were fetched. </returns>
    Public MustOverride Function FetchRelatedEntities(ByVal transaction As TransactionBase) As Boolean

    ''' <summary> Refetches the active entity. Also fetches related entities. Override using a full
    ''' fetch if the entity has a <see cref="SD.LLBLGen.Pro.ORMSupportClasses.IPrefetchPath">prefetch
    ''' path</see>. </summary>
    ''' <remarks> Publishes the <see cref="EntitySaved">entity saved</see> and
    ''' <see cref="EntityChanged">entity changed</see> events. </remarks>
    ''' <returns> <c>True</c> if entity was refetched. </returns>
    Public MustOverride Function Refetch() As Boolean

    ''' <summary> Saves the active entity. </summary>
    ''' <returns> <c>True</c> if entity was save and successfully refetched. </returns>
    Public MustOverride Function Save() As Boolean

#End Region

#Region " EVENT HANDLERS "

#Region " ENTITY CREATED "

    ''' <summary> Notifies that a new entity was created. </summary>
    ''' <remarks> This allows assigning the entity to respond to property changes. </remarks>
    Public Overridable Sub OnEntityCreated()
        Me.OnEntityCreated(EventArgs.Empty)
    End Sub

    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnEntityCreated(ByVal e As System.EventArgs)
        Me.SyncNotifyEntityCreated(e)
    End Sub

    ''' <summary> Removes the EntityCreated event handlers. </summary>
    Protected Sub RemoveEntityCreatedEventHandlers()
        Me._EntityCreatedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The EntityCreated event handlers. </summary>
    Private ReadOnly _EntityCreatedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in EntityCreated events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event EntityCreated As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._EntityCreatedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._EntityCreatedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._EntityCreatedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or
    ''' invokes the <see cref="EntityCreated">EntityCreated Event</see>. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyEntityCreated(ByVal e As System.EventArgs)
        Me._EntityCreatedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " ENTITY CHANGED "

    ''' <summary> Updates the application entity and synchronously send the event. </summary>
    ''' <remarks>
    ''' David 2014-01-30. Disable auto fetch while updating the entity. Changed from a-sync to sync.
    ''' <para>
    ''' David, 2011-05-30, 2.1.4167. fixes bug in restoring auto fetch. </para>
    ''' </remarks>
    Public Overridable Sub OnEntityChanged()
        Me.FetchRelatedEntities()
        Me.OnEntityChanged(EventArgs.Empty)
    End Sub

    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnEntityChanged(ByVal e As System.EventArgs)
        Me.SyncNotifyEntityChanged(e)
    End Sub

    ''' <summary> Removes the EntityChanged event handlers. </summary>
    Protected Sub RemoveEntityChangedEventHandlers()
        Me._EntityChangedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The EntityChanged event handlers. </summary>
    Private ReadOnly _EntityChangedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in EntityChanged events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event EntityChanged As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._EntityChangedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._EntityChangedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._EntityChangedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or
    ''' invokes the <see cref="EntityChanged">EntityChanged Event</see>. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyEntityChanged(ByVal e As System.EventArgs)
        Me._EntityChangedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " ENTITY SAVED "

    ''' <summary> Notifies that a new entity was Saved. </summary>
    ''' <remarks> This allows assigning the entity to respond to property changes. </remarks>
    Public Overridable Sub OnEntitySaved()
        Me.OnEntitySaved(EventArgs.Empty)
    End Sub

    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnEntitySaved(ByVal e As System.EventArgs)
        Me.SyncNotifyEntitySaved(e)
    End Sub

    ''' <summary> Removes the EntitySaved event handlers. </summary>
    Protected Sub RemoveEntitySavedEventHandlers()
        Me._EntitySavedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The EntitySaved event handlers. </summary>
    Private ReadOnly _EntitySavedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in EntitySaved events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event EntitySaved As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._EntitySavedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._EntitySavedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._EntitySavedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary> Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or
    ''' invokes the <see cref="EntitySaved">EntitySaved Event</see>. </summary>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyEntitySaved(ByVal e As System.EventArgs)
        Me._EntitySavedEventHandlers.Send(Me, e)
    End Sub

#End Region

#End Region

#Region " ENTITY PROPERTY CHANGED IMPLEMENTATION "

    ''' <summary> Event handler. Called by  for  events. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>True</c> if this field is primary key field; otherwise, <c>False</c>. </returns>
    Public Function IsPrimaryKeyField(ByVal value As String) As Boolean
        Return Not String.IsNullOrWhiteSpace(value) AndAlso Me._Entity.Fields(value) IsNot Nothing AndAlso Me._Entity.Fields(value).IsPrimaryKey
    End Function

    ''' <summary> Raises the property changed event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnEntityPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If e IsNot Nothing Then
            Me.SyncNotifyEntityPropertyChanged(e)
            ' This causes a cross-thread exceptions.
            ' Me.AsyncNotifyEntityPropertyChanged(e.PropertyName)
        End If
    End Sub

    ''' <summary> Handles a change in the entity property. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    Private Sub EntityPropertyChangedHandler(ByVal sender As Object, ByVal e As PropertyChangedEventArgs) Handles _Entity.PropertyChanged
        Me.OnEntityPropertyChanged(e)
    End Sub

    ''' <summary> Removes the property changed event handlers. </summary>
    Protected Sub RemoveEntityPropertyChangedEventHandlers()
        For i As Integer = Me._EntityPropertyChangedHandlers.Count - 1 To 0 Step -1
            Me._EntityPropertyChangedHandlers.RemoveAt(i)
        Next
    End Sub

    ''' <summary> The entity property changed handlers. </summary>
    Private ReadOnly _EntityPropertyChangedHandlers As New PropertyChangeEventContextCollection()

    ''' <summary> Event queue for all listeners interested in Custom events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event EntityPropertyChanged As PropertyChangedEventHandler

        AddHandler(ByVal value As PropertyChangedEventHandler)
            Me._EntityPropertyChangedHandlers.Add(New PropertyChangedEventContext(value))
        End AddHandler

        RemoveHandler(ByVal value As PropertyChangedEventHandler)
            Me._EntityPropertyChangedHandlers.RemoveValue(value)
        End RemoveHandler

        RaiseEvent(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Me._EntityPropertyChangedHandlers.Post(sender, e)
        End RaiseEvent

    End Event

    ''' <summary>
    ''' Synchronously notifies (sends or safely begins invokes on targets or dynamically invokes) a
    ''' change <see cref="PropertyChanged">event</see> in entity property.
    ''' </summary>
    ''' <remarks>
    ''' Includes a work around because for some reason, the binding write value does not occur. This
    ''' is dangerous because it could lead to a stack overflow. It can be used if the property
    ''' changed event is raised only if the property changed.
    ''' </remarks>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Sub SyncNotifyEntityPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.SendEntityPropertyChanged(e)
    End Sub

    ''' <summary> Synchronously notifies (sends or safely begins invokes on targets or dynamically
    ''' invokes) a change <see cref="PropertyChanged">event</see> in entity property. </summary>
    ''' <remarks> Includes a work around because for some reason, the binding write value does not
    ''' occur. This is dangerous because it could lead to a stack overflow. It can be used if the
    ''' property changed event is raised only if the property changed. </remarks>
    ''' <param name="name"> The name. </param>
    Protected Sub SyncNotifyEntityPropertyChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then Me.SyncNotifyEntityPropertyChanged(New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Asynchronously notifies (posts or safely begins invokes on targets or dynamically
    ''' invokes) a change <see cref="PropertyChanged">event</see> in entity property. </summary>
    ''' <remarks> Includes a work around because for some reason, the binding write value does not
    ''' occur. This is dangerous because it could lead to a stack overflow. It can be used if the
    ''' property changed event is raised only if the property changed. </remarks>
    Protected Sub AsyncNotifyEntityPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.PostEntityPropertyChanged(e)
    End Sub

    ''' <summary> Asynchronously notifies (posts or safely begins invokes on targets or dynamically
    ''' invokes) a change <see cref="PropertyChanged">event</see> in entity property. </summary>
    ''' <remarks> Includes a work around because for some reason, the binding write value does not
    ''' occur. This is dangerous because it could lead to a stack overflow. It can be used if the
    ''' property changed event is raised only if the property changed. </remarks>
    ''' <param name="name"> The name. </param>
    Protected Sub AsyncNotifyEntityPropertyChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then Me.AsyncNotifyEntityPropertyChanged(New PropertyChangedEventArgs(name))
    End Sub

#Region " NOTIFY PROPERTY CHANGE "

    ''' <summary> Gets or sets the publishable. </summary>
    ''' <value> The publishable. </value>
    Public Property Publishable As Boolean

    ''' <summary> Resume publishing. </summary>
    Public Sub ResumePublishing()
        Me.Publishable = True
    End Sub

    ''' <summary> Suspend publishing. </summary>
    Public Sub SuspendPublishing()
        Me.Publishable = False
    End Sub

    ''' <summary> Notifies the properties changed. </summary>
    Public Overridable Sub AsyncNotifyPropertiesChanged()
    End Sub

    ''' <summary>
    ''' Executes (sends) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. synchronously (thread safe)
    ''' </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    '''                  data. </param>
    Private Sub SendEntityPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then Me._EntityPropertyChangedHandlers.Send(Me, e)
    End Sub

    ''' <summary>
    ''' Executes (posts) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. asynchronously (thread unsafe)
    ''' </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub PostEntityPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then Me._EntityPropertyChangedHandlers.Post(Me, e)
    End Sub

#End Region

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> Declared as must override so that the Trace Event Id of the caller library is used </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> Declared as must override so that the <see cref="ToFullBlownString(Exception)"/> is extended based on the caller library implementation </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

