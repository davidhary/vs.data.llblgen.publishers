Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Constructor that prevents a default instance of this class from being created. </summary>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = ProjectTraceEventId.Publishers

        Public Const AssemblyTitle As String = "LLBLGen Publishers Library"
        Public Const AssemblyDescription As String = "LLBLGen O/R Mapping Publishers Library"
        Public Const AssemblyProduct As String = "LLBLGen.Publishers"

    End Class

    ''' <summary> Values that represent project trace event identifiers. </summary>
    Public Enum ProjectTraceEventId
        <System.ComponentModel.Description("Not specified")> None
        <System.ComponentModel.Description("LLBLGen Publishers Library")> Publishers = isr.Data.LLBLGen.My.ProjectTraceEventId.LLBLGenCore + &H1
        <System.ComponentModel.Description("LLBLgen Self Servicing Publishers")> SelfServicing = ProjectTraceEventId.Publishers + &H1
        <System.ComponentModel.Description("LLBLgen Publishers Tester")> PublishersTester = ProjectTraceEventId.Publishers + &HA
    End Enum

End Namespace

